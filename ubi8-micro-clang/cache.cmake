# This file sets up a CMakeCache for an LLVM-only Linux toolchain build.
#
# The nested runtimes build requires libc++, libc++abi and libunwind to be
# installed on the host because the just-built clang uses libc++ by
# default.

# Needed to add stage1 dependency on lld.
set(BOOTSTRAP_LLVM_ENABLE_LLD ON CACHE BOOL "")

# Setup the bootstrap build.
set(CLANG_BOOTSTRAP_CMAKE_ARGS -C ${CMAKE_CURRENT_LIST_FILE} CACHE STRING "")
set(CLANG_ENABLE_BOOTSTRAP ON CACHE BOOL "")

# Configure clang to use LLVM's tools and libraries by default.
set(CLANG_DEFAULT_CXX_STDLIB libc++ CACHE STRING "")
set(CLANG_DEFAULT_LINKER lld CACHE STRING "")
set(CLANG_DEFAULT_OBJCOPY llvm-objcopy CACHE STRING "")
set(CLANG_DEFAULT_RTLIB compiler-rt CACHE STRING "")
set(CLANG_DEFAULT_UNWINDLIB libunwind CACHE STRING "")

# Configure runtimes to use LLVM's libraries.
set(COMPILER_RT_USE_BUILTINS_LIBRARY ON CACHE BOOL "")
set(LIBCXXABI_USE_COMPILER_RT ON CACHE BOOL "")
set(LIBCXXABI_USE_LLVM_UNWINDER ON CACHE BOOL "")
set(LIBCXX_CXX_ABI libcxxabi CACHE STRING "")
set(LIBCXX_USE_COMPILER_RT ON CACHE BOOL "")
set(LIBUNWIND_USE_COMPILER_RT ON CACHE BOOL "")

# Enable all runtimes.
set(LLVM_ENABLE_RUNTIMES
    compiler-rt
    libcxx
    libcxxabi
    libunwind
    CACHE STRING "")

# Tools to install.
set(LLVM_TOOLCHAIN_TOOLS
    llvm-ar
    llvm-cov
    llvm-cxxfilt
    llvm-lib
    llvm-nm
    llvm-objcopy
    llvm-objdump
    llvm-profdata
    llvm-ranlib
    llvm-rc
    llvm-readobj
    llvm-size
    llvm-strings
    llvm-strip
    llvm-symbolizer
    # symlink version of some of above tools that are enabled by
    # LLVM_INSTALL_BINUTILS_SYMLINKS.
    addr2line
    ar
    c++filt
    nm
    objcopy
    objdump
    readelf
    ranlib
    size
    strings
    strip
    CACHE STRING "")

# Configure builtins.
set(BUILTINS_CMAKE_ARGS
    -DLLVM_ENABLE_PER_TARGET_RUNTIME_DIR=${LLVM_ENABLE_PER_TARGET_RUNTIME_DIR}
    -DLLVM_LIBDIR_SUFFIX=${LLVM_LIBDIR_SUFFIX}
    -DLLVM_LINK_LLVM_DYLIB=${LLVM_LINK_LLVM_DYLIB}
    CACHE STRING "")

# Configure runtimes.
set(RUNTIMES_CMAKE_ARGS
    -DLLVM_ENABLE_MODULES=${LLVM_ENABLE_MODULES}
    -DLLVM_ENABLE_PER_TARGET_RUNTIME_DIR=${LLVM_ENABLE_PER_TARGET_RUNTIME_DIR}
    -DLLVM_LIBDIR_SUFFIX=${LLVM_LIBDIR_SUFFIX}
    -DLLVM_LINK_LLVM_DYLIB=${LLVM_LINK_LLVM_DYLIB}
    CACHE STRING "")
